NCSA Mosaic
===========

This is NCSA Mosaic 2.7, one of the first graphical web browsers.
If you're on Ubuntu or something like it, your time machine is fueled
up and ready to go.  Follow the instructions below to build and run.

Many thanks to [Sean MacLennan and Alan Wylie](https://web.archive.org/web/20120915154245/seanm.ca/mosaic/) for doing the heavy lifting.  And, of course, hats off to Marc Andreessen, Eric Bina, and the rest of the [NCSA](http://www.ncsa.illinois.edu/) team for kicking things off for us.  Thanks!

Building
--------

* First, install these packages:

      sudo apt-get install build-essential libmotif-dev libjpeg62-dev libpng12-dev x11proto-print-dev libxmu-headers libxpm-dev libxmu-dev

* Next, build with:

      make linux

* Run!

      src/Mosaic

## macOS notes:
By default this will look for and use homebrew's libraries, and if they're not installed it'll use MacPorts libraries.
You need to install openmotif and xorg-server from either homebrew or MacPorts, or manually install them to /usr/local!

Here's the build instructions for macOS:

* Install these packages:
```
brew install openmotif xorg-server # If using homebrew
sudo port install openmotif xorg-server # If using MacPorts
```

* Next, build with:
```
make macos
```

